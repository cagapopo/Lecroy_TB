__doc__ = "Monitor data from infiniium oscilloscope, recorded with get_allseg.c"
__author__ = "Bruno Lenzi"
__date__ = "Oct 2016"

# Colors of the scope: yellow, green, blue, red

import os, numpy as np, itertools, sys, time
import matplotlib; matplotlib.use("TkAgg")
from matplotlib import pyplot as plt
plt.ion()
# from optparse import OptionParser

# Path of current script
import inspect
path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

# Add path of current script to PYTHONPATH
sys.path.append( path )


def zeroCrossings(data):
  "Return the indices where the 1-D array data changes sign"
  pos = np.greater(data, 0)
  npos = ~pos
  return ((pos[:-1] & npos[1:]) | (npos[:-1] & pos[1:])).nonzero()[0]
#   signs = np.sign(data)
#   signs[signs == 0] == -1
#   return np.where(numpy.diff(signs))[0]  

def beginningOfPulse(data, peak=None):
  """Return the index where the pulse starts, roughly 
  (the last point where it crosses 0 before the peak)"""
  if peak is None:
    peak = abs(data).argmax()
  return zeroCrossings[data[:peak]][-1]

def CFD(data, delta, peak = None):
  """CFD(data, delta) -> float
  Return the position of the pulse using a constant fraction discriminator with
  delay delta (in number of positions)"""
  from scipy.ndimage.interpolation import shift
  if peak is None:
    peak = abs(data).argmax()
  dataP = data
  #dataP = data[peak-delta:peak+delta]
  CFDdata =  (dataP - 0.25 * shift( dataP, delta ))
  # TODO: find zero crossing
  return CFDdata

#def toa(data)


class Plotter:
  "Base class to plot data and update it"
  def __init__(self, reader, manager=None, label=None):
    self.reader = reader
    self.manager = manager
    self.label = label or self.__class__.__name__
    self.subplots = None
    self.setup()
  
  def setup(self):
    "Define figure and subplots. By default just a figure"
    self.fig = plt.figure(self.label)
    self.fig.clf() # clear in case figure exists
  
  def plot(self):
    "Where the magic happens"
    pass
  
  def update(self):
    plt.figure(self.fig.get_label())
    for subplot in self.fig.get_axes():
      subplot.clear()
    self.plot()

class PlotEventsPerCycle(Plotter):
  def setup(self):
    self.fig, self.subplots = plt.subplots(2, 1, num='Events per cycle')
  
  def plot(self):
    evts = self.reader.evtsPerCycle
  
    ax1, ax2 = self.subplots
    ax1.set_title('Last 10 cycles (events: {0})'.format(sum(evts[-10:])) )
    ax1.set_ylabel('Events / cycle')
    ax1.plot( evts[-10:], 'o-' )
    
    ax2.set_title('All cycles (events: {0})'.format(sum(evts)) )
    ax2.set_ylabel('Events / cycle')
    ax2.plot( evts, 'o-' )
    self.fig.canvas.draw()
    self.fig.canvas.flush_events()

class PlotPulses(Plotter):
  def setup(self):
    self.fig, self.subplots = plt.subplots(3, self.reader.Nchannels,
      num='Pulses')
  
  def plot(self):
    ds = self.reader.dataSummary
    inputs = (i for j in (ds.randomPulses, ds.meanPulses, ds.meanPulsesAll) for i in j)
    titles = ['Random from last cycle', 'Mean of last cycle', 'Mean of all']
    
    for input, subplot in zip(inputs, self.subplots.flat):
      subplot.plot( input )
      if subplot.rowNum == 0:
        subplot.set_title( 'Channel {0}'.format(subplot.colNum+1) )
      if subplot.colNum == 0:
        subplot.set_ylabel(titles[subplot.rowNum])
    self.fig.canvas.draw()

class PlotPeakHistograms(Plotter):
  def setup(self):
    self.fig, self.subplots = plt.subplots(self.reader.Nchannels, num = 'Peak histograms')
    if not hasattr(self.subplots, '__iter__'):
      self.subplots = [self.subplots]
  
  def plot(self):
    ds = self.reader.dataSummary
    for peak, subplot in zip(ds.peaks, self.subplots):
      subplot.hist( peak, bins=20 )
      subplot.set_xlabel('Peak value (V)')
    self.fig.canvas.draw()

class PlotSignalsPerChannel(Plotter):
  def setup(self):
    self.fig, self.subplot = plt.subplots(num = 'Number of signals per channel')
    self.ax2 = self.subplot.twinx()
    self.ind = np.arange(self.reader.Nchannels)

  def plot(self):      
    Nevts = self.reader.dataSummary.Nevts
    print Nevts
    self.subplot.set_xlim(0,self.reader.Nchannels)    
    self.subplot.set_title('Events: {0}, cycles read: {1}'.format(Nevts, self.reader.cyclesRead))

    self.ax2.cla()
    self.ax2.set_ylim(top=100.*self.reader.dataSummary.NpeaksAboveTh.max()/Nevts)
    self.ax2.set_ylabel('Efficiency (%)')
    
    self.subplot.set_ylabel('Signals above threshold')
    self.subplot.set_xticks( self.ind + 0.4 )
    self.subplot.set_xticklabels(['ch {0}'.format(i+1) for i in self.ind])
    self.bars = self.subplot.bar(self.ind, self.reader.dataSummary.NpeaksAboveTh, width=0.8 )

    self.fig.canvas.draw()

#class PlotAltiroc(Plotter):
#  def setup(self):
#    self.fig, self.subplots = plot.subplots(self.reader.)


class PlotManager:
  "Class to manage and update plots of summary data"
  def __init__(self, reader, plotters = [PlotEventsPerCycle, PlotPulses, PlotSignalsPerChannel], updTime = 1):
    self.reader = reader
    self.updTime = updTime
    self.plotters = [p(reader, self) for p in plotters]
    self.doUpdate = True
    self.lastUpdate = time.time()
  
  def run(self):
    "Monitor changes in txt file and update plots automatically"
    modTime = os.path.getmtime(self.reader.datFile) - 1 # just to force it the first time
    while self.doUpdate:
      if self.reader.cyclesRead < len(self.reader.evtsPerCycle) or \
        os.path.getmtime(self.reader.txtFile) > modTime:
        
        modTime = os.path.getmtime(self.reader.datFile)
        self.update()
        print 'Cycles read {0} / {1}: '.format(self.reader.cyclesRead, len(self.reader.cycleInfo))        
      else:
        time.sleep(self.updTime)
  
  def update(self, doRead = True):
    "Read next cycle and refresh plots"
    self.lastUpdate = time.time()
    if doRead: self.reader.readCycle()
    for plotter in self.plotters:
      plotter.update()

class FileManager:
  "Monitor the newest file in a given path and launch PlotManager accordingly"
  def __init__(self, path, recursive=True):
    self.path = path
    self.newestFile = findNewestFile(self.path, '*.dat', recursive)
    import thread
    from dataReader import DataReader
    while True:
      plt.close('all')
      self.plotManager = PlotManager( DataReader(self.newestFile) )
      thread.start_new_thread(self.watcher, () )      
      print 'Starting monitor with', self.newestFile      
      self.plotManager.run()
      time.sleep(2) # give time for next file
    
      
  def watcher(self):
    while True:
      time.sleep(5)
      nMins = (time.time() - self.plotManager.lastUpdate)/60.
      newestFile = findNewestFile(self.path, '*.dat')
      if nMins > 0.5 and newestFile != self.newestFile and os.path.getsize(newestFile):
        self.newestFile = newestFile
        self.plotManager.doUpdate = False
      elif nMins > 1 and abs(nMins - int(nMins)) < 0.05:
        print 'No updates since {0} min'.format(int(nMins))


def listFiles(directory, pattern='*'):
  """listFiles(directory, pattern=None, recursive=True) -> yield the all file
  in directory that match pattern. Search recursively if requested"""
  import os, fnmatch
  for root, dirs, files in os.walk(directory):
    for basename in files:
        if fnmatch.fnmatch(basename, pattern):
          yield os.path.join(root, basename)
  
def findNewestFile(path, pattern='*', recursive=True):
  """findNewestFile(path, pattern=None, recursive=True) -> return the last modified file
  in path that matches pattern. Search recursively if requested"""
  return max( (i for i in listFiles(path, pattern)), key=os.path.getmtime)


if __name__ == '__main__':
  import user
  from dataReader import DataReader
  if len(sys.argv) < 2:
    sys.argv.append( os.path.join(path, '../data') ) # default path
  if os.path.isdir(sys.argv[1]):
    fm = FileManager(sys.argv[1])
  else:
    r = DataReader(sys.argv[1])
    m = PlotManager(r)
    m.run()
