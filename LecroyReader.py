#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Import Modules                                                             # 
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
import sys, os, string, shutil,pickle, subprocess, math
import getpass
from array import array
from math import *
from time import *
import time
import  numpy as np
from encodings.aliases import aliases
from encodings import codecs
import struct
from itertools import *
import re
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
# Root                                                                       # 
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
import ROOT
from ROOT import *
#from itertools import chain

skipPoints=0

gROOT.SetBatch(1)

#file="Data/data_1529332437.dat"
#txtfile=file.replace(".dat",".txt")

def splitseq(seq,size):
  " Split up seq in pieces of size "
  return [seq[i:i+size] for i in range(0, len(seq), size)]

#def GetNumberFromLine(line):
def getNchannels(txtFile):
	channels=0
	oscillos=0
	with open(txtFile) as f:
		for line in f:
			if line.find("will be saved")>=0:
				print line
				channels+=1
			if line.find("LECROY,WAVERUNNER8104")>=0:
				oscillos+=1
	return channels,oscillos

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

def readBinary(file,cycleInfo,channels,bytesRead,cycle):

  bdat=np.fromfile(file,count=int(cycleInfo['bytesHex']),dtype=np.int16)
  Nchannels=int(channels)
  #print cycleInfo['bytes'],bytesRead,len(bdat)
  #wf_size=bytesRead/(channels*cycleInfo)
  timeList=[]
  voltList=[]
  adcList=[]
  head=0
  firsthead=0
  start=0
  trl=0
  #print cycleInfo
  totalPointsInCycle=0
  for ichan in range(0,int(channels)):
    #print cycleInfo['VERTICAL_OFFSET'][ichan]
    Npoints=int(cycleInfo['WAVE_ARRAY_1'][ichan]/(2*cycleInfo['NOM_SUBARRAY_COUNT'][ichan]))
    #start=int(40+bytesRead+ichan*(cycleInfo['bytes']/Nchannels))
    #start=int(firsthead+ichan*(cycleInfo['bytes']/Nchannels))
    start+=firsthead+trl
    #start=int(firsthead)
    #print start,ichan,Npoints,cycleInfo['bytes']

    for ipulse in range(0,int(cycleInfo['NOM_SUBARRAY_COUNT'][ichan])):
      timeList_singlePulse=[]
      voltList_singlePulse=[]				
      for i in range(start,start+Npoints-skipPoints):
        time = (i-start)*cycleInfo['HORIZ_INTERVAL'][ichan]+cycleInfo['HORIZ_OFFSET'][ichan]
        volt = 1000*(cycleInfo['VERTICAL_GAIN'][ichan]*bdat[i]-cycleInfo['VERTICAL_OFFSET'][ichan])
        timeList.append(time)
        voltList.append(volt)
        adcList.append(bdat[i])
        #if bdat[i]==127:
           #print i,i-start,time,bdat[i],volt
        
        timeList_singlePulse.append(time)
        voltList_singlePulse.append(volt)

        
        totalPointsInCycle+=1
      start+=Npoints+head
      

      #tArray_singlePulse = np.array(timeList_singlePulse)
      #vArray_singlePulse = np.array(voltList_singlePulse)
      #c1=TCanvas()
      #g = TGraph(len(tArray_singlePulse),tArray_singlePulse,vArray_singlePulse)
      #g.Draw()
      #c1.SaveAs("pulse"+str(cycle)+"_"+str(ipulse)+"_"+str(ichan)+".gif")

    #trl=3

  tArray = np.array(timeList)
  adcArray = np.array(adcList)
  vArray = np.array(voltList)
  #print totalPointsInCycle
  #print xArray
  #g = TGraph(len(tArray),tArray,vArray)
  #g.Draw("AP")
  #nb = raw_input('Choose a number: ')
  return vArray,tArray,adcArray




filename=1529342089
datapath = 'Data/'



class DataReader:
#  """Class to read data recorded via get_allseg.c"""
  def __init__(self, file, doSummary = True):
    try:
      timestamp = int(file) # timestamp given instead of filename
      basepath = os.path.expanduser(datapath)
      oscPattern = 'data_{0}'
      filename = os.path.join(basepath, oscPattern.format(timestamp))
    except ValueError:
      filename, extension = os.path.splitext(file)
      if extension not in ['', '.txt', '.dat']:
        raise IOError('Invalid extension: %s' % extension)

    self.txtFile = filename + '.txt'
    self.datFile = filename + '.dat'
    if not all(map(os.path.isfile, (self.txtFile, self.datFile))):
      raise IOError('Could not find txt and/or dat files ({0}, {1}'.format(self.txtFile, self.datFile))

    self.txtBytesRead = 0 # bytes read in txt file
    self.Nchannels, self.Nscopes = getNchannels(self.txtFile)
    print self.Nchannels, self.Nscopes,"====="

    self.info = []
    self.cycleInfo = []
    self.evtsPerCycle = []
    #self.trigTimeArray=[]
    self.txtlinesRead=0
    self.readTxt()
    self.cyclesRead = 0
    self.doSummary = doSummary
    self.dataSummary = None
    
    self.infoDic={}
    

  def readTxt(self):
    "readTxt(self, update=False) -> read txt file and update information in self.info"
    if os.path.getsize(self.txtFile) - self.txtBytesRead < 350 * self.Nchannels:
      return False
    with open(self.txtFile) as f:
      import ast
#       f.seek(self.txtBytesRead)
#       newInfo = map(ast.literal_eval, f)
      lines = f.readlines()

      #newInfo = map(ast.literal_eval, lines[len(self.evtsPerCycle)*self.Nchannels:])
      self.infoDic=self.ReadWaveDesc(lines[len(self.evtsPerCycle)*self.Nchannels+1+2*self.Nchannels:])
      newInfo = np.array([self.infoDic[key] for key in self.infoDic.keys()]).T
      if len(newInfo) < self.Nchannels:
        return False
        raise IOError('Only read {0} lines of txt file instead of {1}'.format(len(newInfo), self.Nchannels))
      newCycleInfo = self.getCycleInfo(newInfo)
      if newCycleInfo is None: return False        
#         return self.readTxt()
      self.txtBytesRead = f.tell() # position in file
    
    self.info.extend( newInfo )
    self.evtsPerCycle.extend( [i['NOM_SUBARRAY_COUNT'] for i in newCycleInfo] )
    self.cycleInfo.extend( newCycleInfo )
    return True


  def ReadWaveDesc(self,lines):
    #allInfoArr=[]
    allInfo={}
    allInfo["VERTICAL_GAIN"]=[]
    allInfo["WAVE_ARRAY_1"]=[]
    allInfo["NOM_SUBARRAY_COUNT"]=[]
    allInfo["VERTICAL_OFFSET"]=[]
    allInfo["HORIZ_INTERVAL"]=[]   
    allInfo["HORIZ_OFFSET"  ]=[]
    #allInfo["TRIGGER_TIME"  ]=[]


    timeInfo=[]
    lineList=[]

    for line,iline in zip(lines,range(0,len(lines))):
        line=line.strip()
        line=line.split(";")
        #sys.exit()
        for l in line:
          for key in allInfo.keys():
              if l.find(key)>=0:
                l=l.replace(key,"").replace(":","").replace(r'"',"")
                l=l.strip()
                if key!="TRIGGER_TIME":
              	   allInfo[key].append(float(l))
                else:
              	   allInfo[key].append(str(l))
    print allInfo
    return allInfo

  def getCycleInfo(self, info):
    """Given a list of info (metadata), return a list of dictionaries with information
    corresponding to each cycle"""
    fields = dict(Nsamples=2, deltaT=4, T0=5, deltaV=7, V0=8, Nevts=24)
    singleFields = ['']
    arrayFields = 'VERTICAL_GAIN','HORIZ_INTERVAL', 'HORIZ_OFFSET', 'NOM_SUBARRAY_COUNT', 'VERTICAL_OFFSET', 'WAVE_ARRAY_1'
    nestedList = [zip(*i) for i in splitseq(info, self.Nchannels)]
    infoDict = [dict( (key, values[index]) for key, index in zip(self.infoDic.keys(),range(0,len(self.infoDic.keys())))) for values in nestedList]
#    # Append bytes per cycle
    for d in infoDict:
      for key, values in d.iteritems():
        if len(values) != self.Nchannels:
          #(key in singleFields and not all(i == values[0] for i in values)):
            return None
        if key in singleFields:
          d[key] = values[0]
          v = values[0]
          if not all(i == v for i in values):
            raise ValueError('Different values for {key}: {v}'.format(key=key, v=values))
          d[key] = v
        elif len(values) != self.Nchannels:
          return None
          raise ValueError('Length of {key}: {v}, instead of {n}'.format(key=key, v=len(values), n=self.Nchannels))
        if key in arrayFields:
          d[key] = np.array(values)
      #d['bytes']=self.Nchannels*43+sum(d['WAVE_ARRAY_1'])+8*sum(d['NOM_SUBARRAY_COUNT']) ##43=40 bits of header and 3 bits of trail for each waveform
      d['bytes']=sum(d['WAVE_ARRAY_1']) ##43=40 bits of header and 3 bits of trail for each waveform
      d['bytesHex']=sum(d['WAVE_ARRAY_1'])/2. ##43=40 bits of header and 3 bits of trail for each waveform
      #print "total bytes =  ", d['bytes']
      

    return infoDict
##  
  def readCycle(self, cycle = -1):
    """readCycle(self, cycle = -1) -> Read data from next (or given) cycle and save it in
     self.pulses, self.times"""
    # make sure there are new cycles to be read and txt file is up to date
    if cycle == -1 and self.cyclesRead == len(self.cycleInfo) and not self.readTxt(): 
      return False # No more cycles to be read
    # Determine the number of bytes to skip to reach the desired cycle
    if cycle == -1: 
      cycle = self.cyclesRead # there might be more cycles in cycleInfo than the ones read
    bytesRead = sum(i['bytes'] for i in self.cycleInfo[:cycle])
    Nchannels = self.Nchannels
    cycleInfo = self.cycleInfo[cycle]
    fields = 'VERTICAL_GAIN','HORIZ_INTERVAL', 'HORIZ_OFFSET', 'VERTICAL_OFFSET', 'NOM_SUBARRAY_COUNT', 'WAVE_ARRAY_1'

    deltaV,deltaT,T0,V0,Nevts,Nsamples = map(cycleInfo.__getitem__, fields)

    Nvalues = sum(Nsamples)/2.
    NsamplesHex = np.array([int(n/2) for n in Nsamples])    


    if os.path.getsize(self.datFile) < bytesRead + self.cycleInfo[cycle]['bytes']:
      # Not enough bytes to read. Wait and try again
      #	print "Watiiing"
      time.sleep(0.2)
      return self.readCycle(cycle)

    with open(self.datFile, 'rb') as f:

      f.seek(bytesRead)
      x=np.fromfile(f,count=int(cycleInfo['bytesHex']),dtype=np.int16)
      if len(x)!=Nvalues-skipPoints*sum(Nevts): 
        raise ValueError('Only read %s ADC values instead of %s' % (len(x), Nvalues))
      if np.all(NsamplesHex[0] == NsamplesHex): # use np.array if possible
        self.ADC = x.reshape(int(Nchannels), int(Nevts[0]), int(NsamplesHex[0]/Nevts[0])-skipPoints )
      else:
        #print Nevts[0],[int(n) for n in NsamplesHex.cumsum()], list(int(Nevts[0]) * [int(n) for n in NsamplesHex.cumsum()])#,len(self.ADC)         
        indices = pairwise([0] + list(int(Nevts[0]) * [int(n) for n in NsamplesHex.cumsum()]))
        index=[(i,j) for (i,j) in indices]
        self.ADC=[x[index[chan][0]:index[chan][1]].reshape(int(Nevts[chan]), int(NsamplesHex[chan]/Nevts[chan])-skipPoints) for chan in range(0,len(Nevts))]


    
    # Convert ADC to V
    if isinstance(self.ADC, np.ndarray):
      self.pulses = self.ADC * deltaV[:, np.newaxis, np.newaxis] - V0[:, np.newaxis, np.newaxis]
      #self.pulses = self.ADC #* deltaV[:, np.newaxis, np.newaxis] + V0[:, np.newaxis, np.newaxis]
    else:
      self.pulses = [i * j - k for i,j,k in izip(self.ADC, deltaV, V0)]
    #ReadTrigTimes
    # Time of each sample relative to trigger
    self.t = [t0 + dt * np.arange(n) for t0,dt,n in izip(T0, deltaT, Nsamples)]
    #if np.all(NsamplesHex[0] == NsamplesHex):
    #  self.t = t.reshape(int(Nchannels), int(Nevts[0]), int(NsamplesHex[0]/Nevts[0])-skipPoints )
    #else:
    #  indices = pairwise([0] + list(int(Nevts[0]) * [int(n) for n in NsamplesHex.cumsum()]))
    #  index=[(i,j) for (i,j) in indices]
    #  self.t=[t[index[chan][0]:index[chan][1]].reshape(int(Nevts[chan]), int(NsamplesHex[chan]/Nevts[chan])-skipPoints) for chan in range(0,len(Nevts))]
    
    #Newpulses=np.delete(self.pulses,-1,2)
    #Newt=np.delete(self.t,-1,2)
    if cycle == self.cyclesRead: # reading cycles in sequence
      self.cyclesRead += 1
    if self.doSummary:
      self.dataSummary = DataSummary(self, self.dataSummary)
    return True

        

        
  def append(self, pulses, times):
    "Append pulses, times, info to existing objects"
    self.pulses = np.concatenate( [self.pulses, pulses], axis=1)
    self.times = np.concatenate( [self.times, times] )
  
  #self.readCycle(1)
  def readAllCycles(self):
    "Read all cycles and store, pulses, times and info (metadata)"
    #self.readCycle(1)
    pulses, times = zip(*(self.readCycle() for i,j in enumerate(self.cycleInfo)))
    self.pulses = np.concatenate( pulses, axis=1)
    self.times = np.concatenate( times )
##
##
class DataSummary:
  "Class to summarise data from one or multiple cycles"
  level = 0
  def __init__(self, reader, ds=None, threshold=-0.0025):
    import random
    pulses = reader.pulses
    
    self.randomPulses = [i[random.randrange(0, i.shape[0])] for i in pulses]
    self.meanPulses = [i.mean(axis=0) for i in pulses]

    self.peakV = np.array([i.min(axis=1) for i in pulses])
    self.peakI = np.array([i.argmin(axis=1) for i in pulses])
    self.NpeaksAboveTh = np.sum(self.peakV < threshold, axis=1)
    self.Nevts = int(reader.cycleInfo[reader.cyclesRead-1]['NOM_SUBARRAY_COUNT'][0]) # counter already updated
    #print self.NpeaksAboveTh, threshold
    self.evtsPerCycle = [self.Nevts]
    self.meanPulsesAll = self.meanPulses
    if ds is not None: # merge info with previous cycles
      #self.meanPulsesAll = [(dsMean * ds.Nevts + mean * self.Nevts) / (ds.Nevts + self.Nevts) for dsMean, mean in zip(ds.meanPulsesAll, self.meanPulsesAll)]
      self.Nevts += ds.Nevts
      self.evtsPerCycle.extend( ds.evtsPerCycle )
      self.NpeaksAboveTh += ds.NpeaksAboveTh
      self.peakV = np.concatenate( [self.peakV, ds.peakV], axis=1 )
      self.peakI = np.concatenate( [self.peakI, ds.peakI], axis=1 )


if __name__ == '__main__':
  import user, sys
  if len(sys.argv) > 1:
    inputfile = sys.argv[1]
    r = DataReader(inputfile)
    print r
  else:
    t = TestDataReader()
